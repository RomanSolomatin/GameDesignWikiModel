Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2016-05-14T13:31:15-03:00

====== Estruturas ======

Contém as construções que afetam a jogabilidade, que possuem algum papel real dentro do jogo. Um exemplo são as estruturas de um RTS, como torres, acampamentos, etc.
