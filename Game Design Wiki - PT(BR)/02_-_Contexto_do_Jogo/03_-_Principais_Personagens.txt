Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2016-05-14T13:17:10-03:00

====== Principais Personagens ======

Descreve os principais personagens do jogo. Deve conter suas características, habilidades, história e o que mais for necessário. Caso haja muitos personagens, novas páginas podem ser criadas para cada um deles.

Ver também: [[03 - Objetos Essenciais:01 - Personagens]]
