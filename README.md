Game Design Wiki

This project is based on the Game Design Document idea from Paul Schuytema's book "Game Design: A Practical Approach".
The project is using a Desktop Wiki software, Zim Wiki, and you can download it and find more information at http://zim-wiki.org/ 
and https://github.com/jaap-karssenberg/zim-wiki/wiki

This is not a definitive Game Design Document, because such thing doesn't exist, but it is a good start if you don't have a model.
There are several itens in this game design wiki, and you shall delete or include itens as you feel the necessity.
If you have suggestions on anything here, just send me a message.

HOW TO USE
- Download Zim Wiki from http://zim-wiki.org/.
- Install it in your computer.
- Clone this repository or simply download it as a zip.
- There may be some language folders, so choose yours and open the notebook.zim file.
- You can edit the sections and the texts in it to best fit your project necessities. 